#include "myUart.h"
#include <Board.h>
#include <ti/drivers/UART.h>
#include <ti/drivers/uart/UARTCC26XX.h>
#include <stdint.h>
#include <xdc/runtime/System.h>
#include <string.h>
#include <stdlib.h>     /* atoi */
#include "csf.h"
#include "uart_printf.h"
UartRx_Typedef_t UartRxType = {0};
extern UART_Handle hUart;
extern dataSend_Typedef_t myData;
static void MYUART_ReceiveCallback(UART_Handle handle, void* rxBuff, size_t size)
{
    // System_printf("%c", ((uint8_t*)rxBuff)[0]);
    UartRxType.buffer[UartRxType.index++] = ((uint8_t*)rxBuff)[0];
    UartRxType.buffer[UartRxType.index] = 0;
    UartRxType.timeout = 3;
    UART_read(hUart, &UartRxType.byteTem, 1);
}
//[AGV_CP,<MAC>,$cmd,data#]
static void MYUART_ProcessNewData(void)
{
    if(UartRxType.buffer[0] == '[' && UartRxType.buffer[UartRxType.index - 1] == ']')
    {
        char *token;
        char *mac;
        uint8_t _4bitH, _4bitL;
        token = strtok((char*)UartRxType.buffer, "[,$#]"); //AGV_CP
        token = strtok(NULL, "[,$#]");    //MAC: 8byte
        mac = token;
        for(uint8_t i = 0; i < 8; i++)
        {
            _4bitH = token[2*i];
            _4bitL = token[2*i+1];
            if('0' <= _4bitH && _4bitH <= '9') _4bitH -= '0'; 
            else if('a' <= _4bitH && _4bitH <= 'f') _4bitH -= 87; 
            if('0' <= _4bitL && _4bitL <= '9') _4bitL -= '0';
            else if('a' <= _4bitL && _4bitL <= 'f') _4bitL -= 87; 
            myData.MAC[7 - i] = _4bitH*16 + _4bitL;
        }
        token = strtok(NULL, "[,$#]");    //cmd
        token = strtok(NULL, "[,$#]");    //data
        myData.data = atoi((const char*)token);
        // System_printf("mac: %s, data %d\n", mac, myData.data);
        CustomCollector_sendData();
    }
}

void MYUART_Init(uint32_t baudRate)
{
    UART_Params uartParams;
    UART_init();
    UART_Params_init(&uartParams);
    uartParams.baudRate = baudRate;
    uartParams.writeDataMode = UART_DATA_BINARY;
    uartParams.readDataMode = UART_DATA_BINARY;
    uartParams.readReturnMode = UART_RETURN_FULL;
    uartParams.readEcho = UART_ECHO_OFF;

    uartParams.readMode = UART_MODE_CALLBACK;
    uartParams.readCallback = MYUART_ReceiveCallback;
    uartParams.writeCallback = NULL;
    UartPrintf_init(UART_open(Board_UART0, &uartParams));
    UART_read(hUart, &UartRxType.byteTem, 1);
}

void MYUART_Process(void)
{
    if(UartRxType.timeout > 0)
    {
        if(--UartRxType.timeout == 0)
        {   
            MYUART_ProcessNewData();
            UartRxType.index = 0;
        }
    }
}
