#ifndef _MYUART_H
#define _MYUART_H
#include <ti/drivers/UART.h>

typedef struct
{
    uint8_t buffer[50];
    uint8_t byteTem;
    uint8_t index;
    uint8_t timeout;
} UartRx_Typedef_t;

void MYUART_Init(uint32_t baudRate);
void MYUART_Process(void);
#endif
